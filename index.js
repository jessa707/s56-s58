function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if (letter.length !== 1) {
        return undefined;
    }

    // Convert the sentence into an array of characters
    const sentenceChars = sentence.split('');

    // Iterate over each character and count the occurrences of the letter
    for (let i = 0; i < sentenceChars.length; i++) {
        if (sentenceChars[i] === letter) {
            result++;
        }
    }

    return result;
}



// console.log(countLetter(letter, sentence)); 


function isIsogram(text) {

        const lowercaseText = text.toLowerCase();
        
        
        const letterFrequency = {};
        
        
        for (let i = 0; i < lowercaseText.length; i++) {
          const letter = lowercaseText[i];
          
          
          if (letterFrequency[letter]) {
            
            return false;
          } else {
           
            letterFrequency[letter] = 1;
          }
        }
        
        
        return true;
      }
      console.log(isIsogram("jess"));    
      console.log(isIsogram("exam"));    
      console.log(isIsogram("JavaScript"));
      console.log(isIsogram("algorithm")); 
      
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    function purchase(age, price) {
        // Return undefined for people aged below 13.
        if (age < 13) {
            return undefined;
        }
    
        let discountedPrice = price;
        // Calculate the discounted price (rounded off) for students aged 13 to 21 and senior citizens (20% discount)
        if (age >= 13 && age <= 21 || age >= 65) {
            discountedPrice = Math.round(price * 0.8);
        }
        // Return the rounded off price for people aged 22 to 64.
        else if (age >= 22 && age <= 64) {
            discountedPrice = Math.round(price);
        }
    
        // The returned value should be a string with two decimal places.
        return discountedPrice.toFixed(2);
    }
    
    const age = 13;
    const price = 100;
    
    console.log(purchase(age, price)); 
    console.log(purchase(65, price)); 
    console.log(purchase(13, price));
    console.log(purchase(22, price));



    function findHotCategories(items) {
       
   
        const hotCategories = new Set();
     // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
        for (let i = 0; i < items.length; i++) {
            if (items[i].stocks === 0) {
                hotCategories.add(items[i].category);
            }
        }
     // The hot categories must be unique; no repeating categories.
        return Array.from(hotCategories);
    }
    
    const items = [
        // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

        { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
        { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
        { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
        { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
        { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    ];

    console.log(findHotCategories(items)); 
// The expected output after processing the items array is ['toiletries', 'gadgets'].


//======================================================================//


function findFlyingVoters(candidateA, candidateB) {
    const flyingVoters = [];

    // Iterate over each voter in candidateA array
    for (let i = 0; i < candidateA.length; i++) {
        const voter = candidateA[i];

        // Check if the voter also voted for candidateB
        if (candidateB.includes(voter)) {
            flyingVoters.push(voter);
        }
    }

    return flyingVoters;
}

const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

const result = findFlyingVoters(candidateA, candidateB);
console.log(result); 

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};